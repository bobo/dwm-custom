USAGE: dwm-custom-build [options]

General:
  -h show usage
  -V display dwm-custom version info
  -m show an exhaustive man type help
  -R use rescue config
  -D use default dwm version (default=6.1)

Switch config:
  -l           list all config
  -s <CONFIG>  switch to this config
  -i <CONFIG>  info about this config
  -d <CONFIG>  delete this config

Create/Recompile:
  -f <SUFFIX>  target= a flavor with this suffix
  -v <VERSION> target version is changed (default=6.1)



1. MANIFESTE
============

dwm-custom-build has been written to offer a simple/hackable standard to 
install and configure dwm in userspace : as dwm requires to be compiled in 
order to be customized, it doesn't fit with the traditional repository 
approach. It requires the user to compile its own flavor and be able to launch 
it with X11.

Principles
----------

* use standard tools to reduce the number of dependencies
* focus on code readability
* automatize install process
* use $XDG_CONFIG_HOME/dwm-custom-build to store configuration
* take care of readiness to execute with X11
* provide tips and advertise configuration

Usage with a display manager
----------------------------

The project provides also an infrastructure to be able to run your custom dwm 
with a display manager (gdm, kdm, slim, lightdm) It basically consists in two 
files:
* dwm-custom.desktop
* dwm-custom-session

Project official page: https://git.framasoft.org/bobo/dwm-custom


2. TIPS
=======

Configuration summary
---------------------

To go in the config directory
  $ cd $CONFIGDIR

What to do to change configuration
* modify the config.h
* link .diff patches that were added in the 'patches' directory

Please find community patches at http://dwm.suckless.org/patches/

Configuration through config.h
------------------------------

$CONFIGDIR/config.h contains some settings for dwm This is directly C code Feel 
free to edit it to customize your dwm, then re-run dwm-custom-build

More functionalities through patches
------------------------------------

Please find community patches at http://dwm.suckless.org/patches/
Downloaded patches should be placed in $CONFIGDIR/patches All .diff files in 
the config directory will be considered patches Links them :
  $ cd $CONFIGDIR
  $ ln -s patches/<patche_name> [name.diff]

Make startx run customized dwm
------------------------------

You need at least such a minimalistic .xinitrc:
  $ echo "exec $HOME/.config/dwm-custom/dwm" > ~/.xinitrc


3. TROUBLESHOOT
===============

Fails to apply patchs
---------------------

Order of .diff links names could influence the success of the patching steps If 
patching fails, change name to get the patch applied first.

As an example:
  $ cd $CONFIGDIR
  $ mv <failing_patch>.diff 05.diff

To check the order of patches:
  $ cd $CONFIGDIR
  $ \ls -1 *.diff

If the problem persists, the patch probably needs some fix

Getting back to a default config.h
----------------------------------

In case of compilation trouble:
  $ cd $CONFIGDIR
  $ mv config.h config.h.broken
  $ dwm-custom-build

At the next dwm-custom-build run:
* default config.h will be used for compilation
* you will be proposed to copy the default config.h

If the compilation succeeds, compare config.h and config.h.broken, and try to 
fix the issue

Removal of suspicious patches
-----------------------------

If the compilation keeps on failing, you should think about removing suspicious 
patches
  $ cd $CONFIGDIR
  $ rm <suspicious_patch>.diff
  $ dwm-custom-build


4. INFORMATION
==============
* Author: Germain Bossu
* License: GPL3
* Version: 0.12
* Date: 2016-11-04 (YYYY-MM-DD)
* Project Page: https://git.framasoft.org/bobo/dwm-custom
