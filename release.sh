#!/bin/sh

if [ !  "$(dirname "$0")" = '.' ]; then
	echo "ERROR: this script must be run locally"
fi

checkscript () {
	if [ -x "$1" ]; then
		echo "=====> script: $1"
		checkbashisms "$1"
		checkbashisms -npfx "$1"
		shellcheck -e SC2002 "$1"
	else
		echo "script $1 cannot be found"
		exit 1
	fi
}

pkgbuild () {
	echo "=== case = $PKGBUILD"
	sed -i -e "s/^md5sums=.*/$(makepkg -g PKGBUILD)/" PKGBUILD
	rm -rf /tmp/PKGBUILD_test
	mkdir /tmp/PKGBUILD_test
	cp PKGBUILD /tmp/PKGBUILD_test
	if [ "$PKGBUILD" = 0 ]; then
		cp "snapshot/dwm-custom-$VERSION.tar.gz" /tmp/PKGBUILD_test
	fi
	cd /tmp/PKGBUILD_test || exit 1
	makepkg
	tree /tmp/PKGBUILD_test/pkg
	if [ "$PKGBUILD" = 1 ]; then
		echo '== Next steps:'
		echo git add PKGBUILD
		echo git commit -m \'update PKGBUILD for "$VERSION".tar.gz\'
		echo git push
		exit 0
	fi
	cd - || exit 1
}

man_build () {
	echo "Manual update : $MANUAL"
	rm -f "$MANUAL"
	{
		cat "$DOCDIR/USAGE"
		echo
		echo
		echo 1. MANIFESTE
		echo ============
		echo
		cat "$DOCDIR/MANIFESTE"
		echo
		echo
		echo 2. TIPS
		echo =======
		cat "$DOCDIR/TIP.settings"
		cat "$DOCDIR/TIP.config.h"
		cat "$DOCDIR/TIP.patches"
		cat "$DOCDIR/TIP.xinitrc"
		echo
		echo
		echo 3. TROUBLESHOOT
		echo ===============
		cat "$DOCDIR/TROUBLESHOOTING.patches"
		cat "$DOCDIR/TROUBLESHOOTING.compilation"
		echo
		echo
		echo 4. INFORMATION
		echo ==============
		echo '* Author: Germain Bossu'
		echo '* License: GPL3'
		echo "* Version: $VERSION"
		echo "* Date: $(date +%Y-%m-%d) (YYYY-MM-DD)"
		echo '* Project Page: https://git.framasoft.org/bobo/dwm-custom'
	} >> "$MANUAL"
}

release () {

	## VERSION collect
	VERSION=$(grep '^pkgver' PKGBUILD | sed -e "s:^pkgver=::")

	## Doc update
	DOCDIR=project/doc
	MANUAL="$DOCDIR/MANUAL"
	echo
	echo Project text files reformat:
	if [ -x reformat.sh ]; then
		./reformat.sh $DOCDIR/*
		./reformat.sh project/rescue/misc/*
	fi
	man_build

	echo
	echo 'Project git page README.md format check:'
	./reformat.sh README.md
	if [ ! $? ]; then
		echo Please take care of the README.md format
		exit 0
	fi

	## Tarball/PKGBUILD building process
	echo
	echo "VERSION (from PKGBUILD) = '$VERSION'"
	echo 'continue ? (yY)'
	read -r ANSWER
	case $ANSWER in
		[yY])
			## tarball check
			if [ -f "snapshot/dwm-custom-$VERSION.tar.gz" ]; then
				echo "ERROR: snapshot/dwm-custom-$VERSION.tar.gz already exists ! PKGBUILD \$pkgver has probably to be updated"
				exit 1
			fi
			## make a tarball
			cd project || exit 1
			tar -cvf "../snapshot/dwm-custom-$VERSION.tar.gz" ./*
			cd - || exit 1
			## check PKGBUILD
			ln -s "snapshot/dwm-custom-$VERSION.tar.gz"
			pkgbuild 0
			rm -f "dwm-custom-$VERSION.tar.gz"
			echo '== Next steps:'
			echo 'check and modify README.md (git add README.md in this case)'
			echo git status
			echo git add snapshot/dwm-custom-"$VERSION".tar.gz PKGBUILD README.md "$MANUAL"
			echo git status
			echo git commit -m \'add snapshot tarball dwm-custom-"$VERSION".tar.gz\'
			echo git push
			exit 0
			;;
		*)	echo "aborted" ; exit 0 ;;
	esac
}

usage () {
	echo "USAGE: ./$0 [options]"
	echo run checks and possibly create a tarball for release
	echo \\-h display help
	echo \\-r create a release
	echo \\-p test PKGBUILD
	exit 0
}

## Init
RELEASE=0
PKGBUILD=0
while getopts "hrp" opt; do
	case $opt in
		h)  usage ;;
		r)  RELEASE=1 ;;
		p)  PKGBUILD=1 ; RELEASE=0 ;;
		\?) echo "Invalid option: -$OPTARG" ;;
  esac
done

## List of checks to be performed
checkscript project/dwm-custom-build
checkscript project/xsession/dwm-custom-session
checkscript project/rescue/misc/WELCOME.sh
checkscript project/rescue/misc/autostart.sh
checkscript release.sh
checkscript reformat.sh


## Publishing options
if [ $RELEASE  = 1 ] ; then release ; fi
if [ $PKGBUILD = 1 ] ; then	pkgbuild 1 ; fi

echo
echo '== Next steps:'
echo "you may now want to run '$0 -r'"
