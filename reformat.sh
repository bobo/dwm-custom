#!/bin/sh

#if [ !  "$(dirname "$0")" = '.' ]; then
#	echo "ERROR: this script must be run locally"
#fi

CPL=80
if [ $# = 0 ]; then
	echo "USAGE: $0 <path>"
	echo file or files in dir will be reformated
	echo by default CPL is "$CPL"
	exit 0
fi

reformat () {
	cat "$1" |
	perl -pe 's/\s*\n/\n/' |							# remove trailing space
	perl -pe 's/^\s*/$1/ unless /^\s*(\$ |\*|-|\n)/' |	# remove space at beginning of line
	perl -pe 's/\t/ /g' |								# replace tabulations with space
	perl -pe 's/(\S) +/$1 /g unless /^\s*-/' |			# remove replace in-line multiple space with " " (exception for USAGE: line matching ^\s*-)
	perl -pe 's/^\s*(\$ )/  $1/' |						# constant space before code symbol "$ "
	perl -pe 's/^\s+\*/   */' |							# constant space before sublist ( "\s+*" pattern)
	perl -pe 's/^\s+-/  -/' |							# constant space before sublist ( "\s+-" pattern)
	perl -0777 -pe 's/(\w)\n(\w|\$)/$1 $2/mg' |			# join paragraphs (cleaner solution may be possible)
	fold -s -w "$CPL" |									# wrap CPL chars per line
	cat > "$2"
}

for file in "$@" ; do
	NAME="$file"
	if [ -d "$file" ]; then
		echo "SKIPPED (dir)  $NAME"
	elif [ -x "$file" ]; then
		echo "SKIPPED (+x)   $NAME"
	elif [ -w "$file" ]; then
		TMPFILE="$(mktemp "$file.XXXXXX" || exit 1)"
		reformat "$file" "$TMPFILE"
		if diff "$file" "$TMPFILE" > /dev/null 2> /dev/null ; then
			echo "UNCHANGED      $NAME"
		else
			if [ $# = 1 ]; then
				echo "DIFF DETECTED  $NAME"
				echo "vimdiff $file $TMPFILE"
				exit 1
			else
				mv "$TMPFILE" "$file"
				echo "REFORMATED     $NAME"
			fi
		fi
		rm -f "$TMPFILE"
	else
		echo "SKIPPED (-w)   $NAME"
	fi
done
