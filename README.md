This project has been written to offer a simple/hackable standard to install 
and configure dwm in userspace : as dwm requires to be compiled in order to be 
customized, it doesn't fit with the traditional repository approach. It 
requires the user to compile its own flavor and be able to launch it with X11.

Principles of dwm-custom-build
==============================
* use standard tools to reduce the number of dependencies
* focus on code readability
* automatize install process
* use $HOME/.config/dwm-custom to store configuration
* custom dwm binary stored in $HOME tree
* take care of readiness for execution with X11
* provide tips and advertise configuration

Other tools
===========
* xsession kit to allow to launch custom dwm from any compliant display manager
* PKGBUILD as an example of package

Snapshot changelog
==================
* 0.12: config directory structure change, patches need to be links to be applied
* 0.11: fix VERSION&FLAVOR, source tarball are now in dir "SRC\_$VERSION"
* 0.10: fix default behavior running dwm-custom-build with empty config dir
* 0.9: fix document rights
* 0.8: fix empty ~/.config/dwm-custom dir at first start-up
* 0.7: fix in the rescue session
* 0.6: full compliance with multiple versions
   * change link approach, now point at configdir
   * option -V to print version of dwm-custom
   * option -s to switch version without recompiling (link edition)
   * option -l list configs
   * option -i to display info about config
   * option -v to set version (default $DEFAULTVERSION)
   * option -f to set flavor (default '')
   * option -D to go in default mode
   * option -d to delete a config
   * first try download version source, if not here, one fail abort
   * final check update due to new link approach
* 0.5: clean up help/manual options
   * -m option displaying something with $PAGER
   * Manual generation within release.sh
   * help messages formatted with a standard number of chars per line
   * change dwm location to $HOME/.config/dwm-custom/\<VERSION\>
   * "current" link in $HOME/.config/dwm-custom + xsession infrastructure update
* 0.4: better rescue session terminal-only (remove dependency to leafpad)
* 0.3: robust dwm-custom-build rescue config
   * launched in case dwm not found in $HOME with dwm-custom-session
* 0.2: dwm-custom-build code made POSIX and "#!/bin/sh"
* 0.1: initial release

Planned future work before 1.0
==============================
* 1.0: final clean-up/adjustments before 1.0
   * a new section in the MANUAL about config directory principle and structure
   * help TROUBLESHOOT.rescue : to debug compilation issues with rescue mode !
   * add a confirm step before build displaying current info + -f option to skip
   * re-read and modify the whole doc directory
   * check\_initrc: rework || remove check\_initrc + update TIP.xinitrc
   * how to suggest share patches with \<VERSION\>? => dwm-custom-patches
   * at config creation, ask for applying \<VERSION\> patches? => too complex

Possible enhancements
=====================
* dwm-custom-build: regression test option to cover all conditions/cases
   * a -t option pointing at a path
   * goal: be able to launch regression with a log
   * skip current dwm link update
   * kind of written log as a fingerprint to be checked before release
* enhanced rescue mode
   * autostart to launch using available terminal emulator
   * choice between available terminal emulators during dwm-custom-build
   * optional MODKEY modification
   * optional application of keycode patch during dwm-custom-build
* add a 'fancy' dwm-custom-build option (like 'rescue')
* dwm-custom-patches help in sorting out patches
   * automatically put patches in \<VERSION\> alongside to the source .tar.gz
   * automatical relink
   * diff based
* integration to handle supported patches in a community way: dwm-custom-patches
   * git infrastructure to get
   * git infrastructure to share patches: run regression test before acceptance
   * submit regression testcases
   * community patches in \<VERSION\>/community
   * check patches identical to community ones and automatically clean-up
* ncurses GUI configuration
